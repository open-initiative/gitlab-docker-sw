# GitLab URL (replace with yours)
external_url 'https://<YOUR_WEB_ADDRESS>'
nginx['enable'] = true
nginx['listen_port'] = 80
nginx['listen_https'] = false
nginx['proxy_set_headers'] = {
  'X-Forwarded-Proto' => 'https',
  'X-Forwarded-Ssl' => 'on'
}
gitlab_rails['initial_root_password'] = File.read('/run/secrets/gitlab_root_password')
gitlab_rails['initial_shared_runners_registration_token'] = File.read('/run/secrets/gitlab-runner-token')
gitlab_rails['lfs_enabled'] = true
gitlab_rails['gitlab_shell_ssh_port'] = 22
letsencrypt['enable'] = false
letsencrypt['auto_renew'] = false

# Gitlab to S3 backup
gitlab_rails['backup_upload_connection'] = {
  'provider' => 'AWS',
  'region' => 'ru-center-rack1',
  'aws_access_key_id' => 'gitlab-backup-connector',
  'aws_secret_access_key' => '<YOUR_SECRET_KEY>',
  'endpoint' => 'https://<YOUR_S3_ADDRESS>',
  'path_style' => true
}
gitlab_rails['backup_upload_remote_directory'] = 'gitlab-backup-storage'

# GitLab Registry URL (replace with yours)
registry_external_url 'https://<YOUR_REGISTRY_ADDRESS>'
gitlab_rails['registry_enabled'] = true
registry['enable'] = true
registry['registry_http_addr'] = "0.0.0.0:5000"
registry_nginx['enable'] = false

### Registry backend storage
registry['storage'] = {
  's3' => {
    'accesskey' => 'gitlab-s3-connector',
    'secretkey' => 'k8xPMUDPNAicz',
    'bucket' => 'gitlab-registry-storage',
    'region' => 'ru-center-rack1',
    'regionendpoint' => 'https://<YOUR_S3_ADDRESS>',
    'path_style' => true
  },
  'redirect' => {
    'disable' => false
  }
}

### GitLab uploads
gitlab_rails['uploads_object_store_enabled'] = true
gitlab_rails['uploads_object_store_remote_directory'] = "gitlab-uploads-storage"
gitlab_rails['uploads_object_store_connection'] = {
  'provider' => 'AWS',
  'region' => 'ru-center-rack1',
  'aws_access_key_id' => 'gitlab-s3-connector',
  'aws_secret_access_key' => 'k8xPMUDPNAicz',
  'endpoint' => 'https://<YOUR_S3_ADDRESS>', # default: nil - Useful for S3 compliant services such as DigitalOcean Spaces
  'path_style' => true # Use 'host/bucket_name/object' instead of 'bucket_name.host/object'
}

### Terraform state
gitlab_rails['terraform_state_object_store_enabled'] = true
gitlab_rails['terraform_state_object_store_remote_directory'] = "gitlab-terraform-storage"
gitlab_rails['terraform_state_object_store_connection'] = {
  'provider' => 'AWS',
  'region' => 'ru-center-rack1',
  'aws_access_key_id' => 'gitlab-s3-connector',
  'aws_secret_access_key' => 'k8xPMUDPNAicz',
  'endpoint' => 'https://<YOUR_S3_ADDRESS>', # default: nil - Useful for S3 compliant services such as DigitalOcean Spaces
  'path_style' => true # Use 'host/bucket_name/object' instead of 'bucket_name.host/object'
}

### Git LFS
gitlab_rails['lfs_object_store_enabled'] = true
gitlab_rails['lfs_object_store_remote_directory'] = "gitlab-lfs-storage"
gitlab_rails['lfs_object_store_connection'] = {
  'provider' => 'AWS',
  'region' => 'ru-center-rack1',
  'aws_access_key_id' => 'gitlab-s3-connector',
  'aws_secret_access_key' => 'k8xPMUDPNAicz',
  'endpoint' => 'https://<YOUR_S3_ADDRESS>', # default: nil - Useful for S3 compliant services such as DigitalOcean Spaces
  'path_style' => true # Use 'host/bucket_name/object' instead of 'bucket_name.host/object'
}

####! Job artifacts Object Store
gitlab_rails['artifacts_object_store_enabled'] = true
gitlab_rails['artifacts_object_store_remote_directory'] = "gitlab-artifacts-storage"
gitlab_rails['artifacts_object_store_connection'] = {
  'provider' => 'AWS',
  'region' => 'ru-center-rack1',
  'aws_access_key_id' => 'gitlab-s3-connector',
  'aws_secret_access_key' => 'k8xPMUDPNAicz',
  'endpoint' => 'https://<YOUR_S3_ADDRESS>', # default: nil - Useful for S3 compliant services such as DigitalOcean Spaces
  'path_style' => true # Use 'host/bucket_name/object' instead of 'bucket_name.host/object'
}

## Package repository
gitlab_rails['packages_object_store_enabled'] = true
gitlab_rails['packages_object_store_remote_directory'] = "gitlab-packages-storage"
gitlab_rails['packages_object_store_connection'] = {
  'provider' => 'AWS',
  'region' => 'ru-center-rack1',
  'aws_access_key_id' => 'gitlab-s3-connector',
  'aws_secret_access_key' => 'k8xPMUDPNAicz',
  'endpoint' => 'https://<YOUR_S3_ADDRESS>', # default: nil - Useful for S3 compliant services such as DigitalOcean Spaces
  'path_style' => true # Use 'host/bucket_name/object' instead of 'bucket_name.host/object'
}

## Dependency proxy
gitlab_rails['dependency_proxy_object_store_enabled'] = true
gitlab_rails['dependency_proxy_object_store_remote_directory'] = "gitlab-proxy-storage"
gitlab_rails['dependency_proxy_object_store_connection'] = {
  'provider' => 'AWS',
  'region' => 'ru-center-rack1',
  'aws_access_key_id' => 'gitlab-s3-connector',
  'aws_secret_access_key' => 'k8xPMUDPNAicz',
  'endpoint' => 'https://<YOUR_S3_ADDRESS>', # default: nil - Useful for S3 compliant services such as DigitalOcean Spaces
  'path_style' => true # Use 'host/bucket_name/object' instead of 'bucket_name.host/object'
}



# GitLab SMTP (replace with yours)
gitlab_rails['smtp_enable'] = true
gitlab_rails['smtp_address'] = "smtp1.<YOUR_DOMAIN>.ru"
gitlab_rails['smtp_port'] = 587
gitlab_rails['smtp_user_name'] = "ServiceSD2@<YOUR_DOMAIN>.ru"
gitlab_rails['smtp_password'] = "$3rv1c3$d2"
gitlab_rails['smtp_domain'] = "<YOUR_DOMAIN>"
gitlab_rails['smtp_authentication'] = "login"
gitlab_rails['smtp_enable_starttls_auto'] = true

# GitLab Active Directory (replace with yours)
gitlab_rails['ldap_enabled'] = true
gitlab_rails['prevent_ldap_sign_in'] = false
gitlab_rails['ldap_servers'] = YAML.load <<-'EOS'
  main: # 'main' is the GitLab 'provider ID' of this LDAP server
    label: 'LDAP'
    host: '<YOUR_HOST_NAME>'
    port: 389
    uid: 'sAMAccountName'
    bind_dn: 'CN=Gitlab_service,OU=Services,OU=OIT,DC=<YOUR_DOMAIN>,DC=ru'
    password: 'G1tl@b_S3rv1c3'
    encryption: 'plain'
    #verify_certificates: true
    #smartcard_auth: false
    active_directory: true
    allow_username_or_email_login: true
    lowercase_usernames: false
    block_auto_created_users: true
    base: 'DC=<YOUR_DOMAIN>,DC=ru'
    timeout: 10
    attributes:
      username: ['sAMAccountName']
      email: ['mail']
      name: 'displayName'
      first_name: 'givenName'
      last_name: 'cn'
    #user_filter: ''
EOS